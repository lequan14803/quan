﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Logs
    {
        [Key]
        public int LogId { get; set; }
        public int TransactionId { get; set; }
        public Transactions? Transactions { get; set; }
        public DateTime LoginDate { get; set; }
        public DateTime LoginTime { get; set; }

    }
}
