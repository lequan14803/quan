﻿using System.ComponentModel.DataAnnotations;

namespace KiemTra.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeId { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        public string Contact { get; set; }
        public string Address { get; set; }
        
        public string UserName { get; set; }
        
        public string Password { get; set; }

    }
}
